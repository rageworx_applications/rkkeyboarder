# RaphKay's keyboarder

* An windows application for testing keyboard IO.
* This is a part of FLTK open source.

## License

* MIT License, see file of "LICENSE".

## Update news

### version 0.1.2.16

 * Supports GDI+ with FLTK-1.4.0.7
 * Automatically supports High-DPI of Windows.

## Previous versions

### version 0.1.2.13

* Mouse, or touch pad error fixed.


### version 0.1.2.12

* Typing performance display fixed for IO interrupt count.

### version 0.1.2.11

* Typing performance measurement now availed.


### version 0.1.1.8

* Included a title image.

### version 0.1.0.0

* First version

## Required libraries

* [FLTK-1.3.5-2-ts](https://github.com/rageworx/fltk-1.3.5-2-ts)
* [libfl_imgtk](https://github.com/rageworx/fl_imgtk)
  
## Supported OS

* Windows
