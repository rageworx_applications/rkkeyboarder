#ifndef __keyboard_h__
#define __keyboard_h__

#include <cstdio>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>

class KbdWindow : public Fl_Window 
{
    public:
        KbdWindow(int w, int h, const char *t = NULL ) 
        : Fl_Window( w, h, t ),
          roller_x( NULL ),
          roller_y( NULL ),
          boxKeyPerfSec( NULL ),
          boxKeyPerfMin( NULL )
        { 
            // do nothing.
        }

    public:
        Fl_Dial*     roller_x;
        Fl_Dial*     roller_y;
        Fl_Box*      boxKeyPerfSec;
        Fl_Box*      boxKeyPerfMin;

    protected:
        int handle(int);
};

#endif /// of __keyboard_h__
