#include <windows.h>
#include <unistd.h>
#include <cstring>
#include <resource.h>

#include <FL/Fl.H>
#include <FL/platform.H>
#include <FL/Fl_draw.H>

#include "keyboard_ui.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

KbdWindow* window = NULL;

////////////////////////////////////////////////////////////////////////////////

// these are used to identify which buttons are which:
void key_cb(Fl_Button*, void*)
{
    
}

void shift_cb(Fl_Button*, void*) 
{
    
}

void wheel_cb(Fl_Dial*, void*) 
{
    
}

// this is used to stop Esc from exiting the program:
int handle(int e) 
{
    return (e == FL_SHORTCUT); // eat all keystrokes
}

int KbdWindow::handle( int msg ) 
{
    if ( msg == FL_MOUSEWHEEL )
    {
        int rxv = roller_x->value();
        int ryv = roller_y->value();

        roller_x->value( rxv + Fl::e_dx * roller_x->step() );
        roller_y->value( ryv + Fl::e_dy * roller_y->step() );
        
        return 1;
    }
    return 0;
}

int main( int argc, char** argv )
{
    Fl::add_handler(handle);
    Fl::scheme( "flat" );
    Fl_Window::default_xclass( "rkkbdrr" );
    
    window = make_window();
    if ( window != NULL )
    {
        window->show();
        
        HICON \
        hIconWindowLarge = (HICON)LoadImage( fl_display,
                                             MAKEINTRESOURCE( IDC_ICON_A ),
                                             IMAGE_ICON,
                                             128,
                                             128,
                                             LR_SHARED );

        HICON \
        hIconWindowSmall = (HICON)LoadImage( fl_display,
                                             MAKEINTRESOURCE( IDC_ICON_A ),
                                             IMAGE_ICON,
                                             16,
                                             16,
                                             LR_SHARED );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );        

        unsigned tickSec = 0;
        unsigned tickMin = 0;
        unsigned keys_p_sec = 0;
        unsigned keys_p_min = 0;
        
        char kpfstr_sec[80] = {0};
        char kpfstr_min[80] = {0};
        
        // loop by FLTK ---
        while ( Fl::wait() ) 
        {
            // update all the buttons with the current key and shift state:
            for ( size_t cnt=0; cnt< window->children(); cnt++ )
            {
                unsigned tickCur = GetTickCount();
                
                Fl_Widget* widget = window->child(cnt);
                
                if ( widget->callback() == (Fl_Callback*)key_cb ) 
                {
                    int arg = widget->argument();
                    
                    if ( !arg ) 
                        arg = widget->label()[0];
                    
                    Fl_Button *btn = (Fl_Button*)widget;
                    
                    int kstate = Fl::event_key(arg);

                    if ( btn->value() != kstate )
                        btn->value(kstate);
                                        
                    // key check for only un-shifted keys.
                    if ( btn->value() )
                    {
                        keys_p_sec ++;
                        keys_p_min ++;

                        if ( ( tickSec == 0 ) && ( tickMin == 0 ) )
                        {
                            tickSec = tickCur;
                            tickMin = tickCur;
                        }
                        else
                        {
                            if ( tickCur >= ( tickSec + 1000 ) )
                            {
                                float diff = (tickCur - tickSec) / 1000.f;
                                
                                snprintf( kpfstr_sec, 80,
                                          "%.3f IO / sec.",
                                          (float)keys_p_sec / diff );
                                
                                window->boxKeyPerfSec->label( kpfstr_sec );
                                window->boxKeyPerfSec->redraw();
                                
                                tickSec = tickCur;
                                keys_p_sec = 0;
                            }
                            
                            if ( tickCur >= ( tickMin + 10000 ) )
                            {
                                float diff = ( ( tickCur - tickMin ) * 6 ) / 1000.f;
                                
                                snprintf( kpfstr_min, 80,
                                          "%.3f IO / min.",
                                          (float)(keys_p_min * 6) / diff );
                                          
                                window->boxKeyPerfMin->label( kpfstr_min );
                                window->boxKeyPerfMin->redraw();

                                tickMin = tickCur;
                                keys_p_min = 0;
                            }
                        }
                    }
                } 
                else 
                if ( widget->callback() == (Fl_Callback*)shift_cb ) 
                {
                    int arg = widget->argument();
                    
                    Fl_Button *btn = (Fl_Button*)widget;
                    
                    int kstate = Fl::event_state(arg);
                    
                    if ( btn->value() != kstate )
                        btn->value(kstate);
                }
            }
        }
    }
    
    return 0;
}
